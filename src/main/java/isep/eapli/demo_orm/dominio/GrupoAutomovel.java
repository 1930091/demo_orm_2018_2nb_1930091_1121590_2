/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author albertosantos
 */
@Entity
public class GrupoAutomovel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    private Integer portas;
    private String classe;

    protected GrupoAutomovel() {

    }

    public GrupoAutomovel(String nome, Integer portas, String classe) {

        if (nome == null || portas == null || classe == null) {
            throw new IllegalStateException();
        }
        this.nome = nome;
        this.portas = portas;
        this.classe = classe;

    }

    public void alterarClasse(String classe) {
        this.classe = classe;
    }

    public void alteraPortas(Integer portas) {
        this.portas = portas;
    }

}
